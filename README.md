# xmlstarlet-rpm-el6

Rebuild of the EPEL 6.x xmlstarlet RPM with an updated upstream release

Changes include:
* Increase of xmlstarlet version to 1.6.1 from upstream
* Minor update to documentation configuration due to changes in upstream tarball

## Source and compiled RPMs
Source and compiled RPMs (x86_64) built from this repo can be found at the following location **USE AT YOUR OWN RISK**

**x86_64 RPMs**
* [xmlstarlet-1.6.1-1.el6.x86_64.rpm](http://www.pixeldrift.net/rpm/x86_64/xmlstarlet-1.6.1-1.el6.x86_64.rpm)

**SRPMs**
* [xmlstarlet-1.6.1-1.el6.src.rpm](http://www.pixeldrift.net/rpm/SRPMS/xmlstarlet-1.6.1-1.el6.src.rpm)